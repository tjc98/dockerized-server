default:
  image: python:3.9

stages:
  - build
  - test
  - dockerize

variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  PYTHON_PACKAGE_DIR: $CI_PROJECT_DIR/.cache/python-packages
  REQUIREMENTS: requirements.txt
  DEV_REQUIREMENTS: dev.requirements.txt

.pythonpath: &pythonpath
  before_script:
    - export PYTHONPATH="$PYTHON_PACKAGE_DIR"

.cache: &cache
  key: $CI_COMMIT_REF_SLUG
  policy: pull
  paths:
    - $PIP_CACHE_DIR
    - $PYTHON_PACKAGE_DIR

build-dependencies:
  stage: build
  cache:
    <<: *cache
    policy: pull-push
  artifacts:
    expire_in: 1 day
    paths:
      - $PIP_CACHE_DIR
      - $PYTHON_PACKAGE_DIR
  before_script:
    - pip install --upgrade pip
    - rm -rf ${PIP_CACHE_DIR} ${PYTHON_PACKAGE_DIR}
    - export PYTHONPATH="$PYTHON_PACKAGE_DIR"
  script:
    - pip install --progress-bar off --no-cache-dir --target ${PYTHON_PACKAGE_DIR} --requirement ${REQUIREMENTS}
    - pip install --progress-bar off --no-cache-dir --target ${PYTHON_PACKAGE_DIR} --requirement ${DEV_REQUIREMENTS}

test:
  stage: test
  cache:
    <<: *cache
  variables:
    FLASK_APP: flaskr
    FLASK_ENV: development
    POSTGRES_DB: db
    POSTGRES_USER: user
    POSTGRES_PASSWORD: password
    POSTGRES_PORT: 5432
    POSTGRES_HOST: postgres
  services:
    - postgres:13.2
  artifacts:
    when: always
    expire_in: 1 day
    paths:
      - htmlcov
      - .coverage
  coverage: '/Total coverage: \d+.\d+%$/'
  <<: *pythonpath
  script:
    - python -m pytest

dockerize:
  stage: dockerize
  when: manual
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    IMAGE_NAME: simple-strop-server
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA
